# Tasks
#TASK 0: -Get input from user: Bank name or ip address to troubleshoot.
#TASK 1: -Read CSV file and fetch bank info ( bank routers mgmt ip, backtoback ip, bank name,credentials).
#TASK 2: -Login using mgmt ip: if failed login to secondary router and login using back to back ip address ( first try aaa credentials if failed try local credentials )
#TASK 3: - (A)ping WAN ip address (B)check EBGP status & Return output.


from pexpect import pxssh
import pexpect
import getpass
import csv
import re

r_ip = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")

def back_to_back(data):
    try:
        ses = data.get("ssh_session")
        auth = data.get("auth")
        d2 = data["device1"]
        ven = data["vendor"]
        print "Trying to login back to back:"+d2
        for au in auth:
            username = au.get("username")
            password = au.get("password")
            ses.sendline("ssh -l "+username+" "+d2)
            ses.expect(["Password:",pexpect.TIMEOUT],timeout=20)
            ses.sendline(password)
            ses.expect(["Password:","#",">","\$",pexpect.TIMEOUT],timeout=20)
            data = ses.before
            #print data
            prd = ses.after
            if prd.find("Password:") != -1:
                print "Back to Back Failed"
                ses.sendcontrol("c")
                ses.sendcontrol("c")
                ses.sendcontrol("c")
            else:
                print "BACK TO BACK SUCCESS: "+d2
                ses.sendline("")
                new_exp = ses.before.strip()+ses.after.strip()
                print new_exp
                return ses,new_exp
    except Exception as e:
        print e
    

def login(hostname='',auth=[],login_timeout=6,etimeout=5):
    # Login to NPCI device , "enable" password check disabled because of aaa conf in NPCI
    if len(auth) > 0:
        for au in auth:
            #print "Trying to Login:"+hostname
            return_typ = None
            username = au.get("username")
            password = au.get("password")
            try:
                s = pxssh.pxssh(options={
                                "StrictHostKeyChecking": "no",
                                "UserKnownHostsFile": "/dev/null"},timeout=login_timeout)
                s.login(hostname, username, password,auto_prompt_reset=False,login_timeout=login_timeout)
                s.logfile = open("loggg.txt", "w")
                # Send enter to get router prompt to check login success
                s.sendline('')
                # expecting cisco , juniper , fortigate prompt 
                s.expect(["#",">","\$",pexpect.TIMEOUT],timeout=etimeout)
                login_chk = s.before.strip()
                if len(login_chk) > 0:
                    host_name = str(login_chk)
                    aftr = s.after
                    if type(aftr) == str:
                        host_name = host_name+aftr.strip()
                    print "Login Success :"+hostname+":"+host_name
                    return s,host_name
                else:
                    print "Not able to reach device:"+hostname
                return "TIMEOUT"
            except pxssh.ExceptionPxssh as e:
                err = str(e)
                if err.find("password refused") != -1:
                    print "Login Failed:"+hostname
                    return_typ = "LOGINFAIL"
                else:
                    print "Error>"+err+":"+hostname
                    return "TIMEOUT"
            except Exception as e:
                #print("Unknown Error"+str(e))
                return "TIMEOUT"
        return return_typ


class mod_check_end_to_end():
    def __init__(self):
        pass;
    def check_bgp_status(self):
        pass;
        
    def cis_check_route(self,d):
        try:
            if d.get("ip") != None:
                ven = d.get("vendor")
                ses = d.get("ssh_session")
                exp = d.get("exp")
                ses.sendline("show ip route "+d.get("ip"))
                ses.expect([exp,pxssh.TIMEOUT],timeout=20)
                v = ses.before
                r = re.findall(r"Known via \".*\"",v)[0]
                if len(r) > 0:
                    rr = r.replace("Known via \"","").replace("\"","")
                    if rr == 'connected':
                        rrr = re.findall(r"directly connected, via .*",v)[0].split("directly connected, via ")[1].strip()
                        return {"type":rr,"nxt_hop":rrr}
                    else:
                        rrr = re.findall(r"\* \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",v)[0].replace("* ","").strip()
                        if rr.find("bgp") != -1:
                            tag = re.findall(r"Tag .*,",v)[0]
                            tag = tag[:-1].split("Tag ")[1]
                            return {"type":rr,"nxt_hop":rrr,"tag":tag}
                        else:
                            return {"type":rr,"nxt_hop":rrr}
        except Exception as e:
            print e

    def cis_check_arp(self,d):
        try:
            ven = d.get("vendor")
            ses = d.get("ssh_session")
            exp = d.get("exp")
            if d.get("ip") != None and exp != None:
                ses.sendline("show arp "+d.get("ip"))
                ses.expect([exp,pxssh.TIMEOUT],timeout=20)
                v = ses.before
                r = re.findall(r"[a-f|0-9]{4}\.[a-f|0-9]{4}\.[a-f|0-9]{4}",v)
                if len(r) > 0:
                    return {"type":"arp","arp":r[0]}
                else:
                    return {"type":"arp","arp":""}
        except Exception as e:
            print e
    
    def get_arp(self):
        pass;
    def check_reach(self,d):
        s = 0
        ses = d.get("ssh_session")
        src = d.get("source_ip")
        des = d.get("destination_ip")
        pro = d.get("proto")
        ven = d.get("vendor")
        exp = d.get("exp")
        if pro == "ping" and exp != None:
            if src == None:
                cmd = "ping "+des
                ses.sendline(cmd)
                ses.expect([cmd,pxssh.TIMEOUT],timeout=20)
                ses.send("\n")
                ses.expect([exp,pxssh.TIMEOUT],timeout=20)
                s = 1
            else:
                ses.send("ping "+des+" source "+src)
                ses.expect([pxssh.TIMEOUT,"ping "+des+" source "+src],timeout=20)
                ses.send("\n")
                ses.expect([pxssh.TIMEOUT,exp],timeout=20)
                s = 1
            if s == 1:
                s = ses.before
                a = s.split("Success rate is ")
                if len(a) > 1:
                    return {"type":"ping","ping":a[1].split(" ")[0]}
            else:
                print ses.before,ses.after
                return None
    
    def check_bgp(self,d):
        try:
           ses = d.get("ssh_session")
           exp = d.get("exp")
           ven = d.get("vendor")
           data = []
           ses.sendline("show ip bgp neighbors | i BGP neighbor is ")
           ses.expect([exp,pxssh.TIMEOUT],timeout=20)
           s = ses.before
           ses.sendline("show ip bgp neighbors | i BGP state")
           ses.expect([exp,pxssh.TIMEOUT],timeout=20)
           ss = ses.before
           s1 = re.findall(r"BGP neighbor is .*link",s)
           s2 = re.findall(r"BGP state = .*",ss)
           if len(s1) == len(s2) and len(s1) > 0:
               for a in xrange(len(s1)):
                   c3 = ""
                   b1 = s1[a].split(",")
                   b2 = b1[0].replace("BGP neighbor is ","").strip()
                   b3 = b1[1].replace("  remote AS ","").strip()
                   b4 = b1[2].strip().strip()
                   c1 = s2[a].split(",")
                   c2 = c1[0].replace("BGP state = ","").strip()
                   if len(c1) > 1:
                       c3 = c1[1].replace(" up for ","").strip()
                   data.append({"neighbor":b2,"as":b3,"bgptype":b4,"bgpstate":c2,"bgpuptime":c3})
           data = {"type":"bgp","data":data}
           return data
        except Exception as e:
            print e

    def cisco_check_interface(self,d):
        try:
           ses = d.get("ssh_session")
           exp = d.get("exp")
           ven = d.get("vendor")
           data = []
           ses.sendline("show ip interface brief")
           ses.expect([exp,pxssh.TIMEOUT],timeout=20)
           ss = ses.before
           ss = ss.split("\n")[2:]  #Need to check this :-(
           for i in ss:
               mask = ""
               ii = ' '.join(i.split())
               ii = ii.split(" ")
               if len(ii) > 5:
                   interface = ii[0]
                   ip = ii[1]
                   admin = ii[4]
                   proto = ii[5]
                   if ip.find(".") != -1:
                       # Valid ip found check mask for the interface
                       ses.sendline("show interfaces "+interface+" | i "+ip)
                       ses.expect([exp,pxssh.TIMEOUT],timeout=20)
                       mask = ses.before
                       
                       mask = mask.split("Internet address is "+ip+"/")[1].strip()
                   data.append({"interface":interface,"ip_addr":ip,"admin_stat":admin,"protocol_stat":proto,"mask":mask})
           data = {"type":"interface","data":data}
           return data
        except Exception as e:
            print e
    def check_source_route(self):
        pass;
    def check_dest_route(self):
        pass;
    
        

# Last Mile Down Check        
        
class lastMileCheck():
    def __init__(self):
        self.mod = mod_check_end_to_end()

    def check_device(self,d):
        # Check Ping & BGP 
        ses = d.get("ssh_session")
        exp = d.get("exp")
        flow = d.get("flow")
        ebgp_up = None
        ibgp_up = None
        db1 = {"nms_ip":"192.168.233.3"}
        # Check Ping
        d.update({"proto":"ping","destination_ip":db1.get("nms_ip"),"source_ip":d.get("source_ip")})
        png = self.mod.check_reach(d)
        if png == None:
            print "PING TO NMS SERVER FAILED"
        else:
            print "\n"+"PING> NMS SERVER REACHABILITY SUCCESS RATE:"+png.get("ping")+"%"
            print ""
        #Check BGP
        bgp = self.mod.check_bgp(d)
        if bgp != None:
            bgp_data = bgp.get("data")
            for d in bgp_data:
                print "BGP> "+d.get("bgptype")+" "+d.get("as")+" "+d.get("bgpstate")+" for "+d.get("bgpuptime")
                print ""
                    
                    
            
            
    def try_ssh(self,data):
        d1 = data["device1"]
        d2 = data["device2"]
        aaa_usr = data["username"]
        aaa_pass = data["password"]
        local_usr = data["username"]
        local_pass = data["password"]
        d1_stat = None
        
        # DEVICE ONE LOGIN CHECK
        d1_ses = login(d1,[{"username":aaa_usr,"password":aaa_pass}])
        if type(d1_ses) != str and d1_ses != None:
            #Login to device1 success
            self.check_device({"ssh_session":d1_ses[0],"vendor":"cisco","exp":d1_ses[1],"flow":"device1","source_ip":d1})
            d1_stat = True
        elif d1_ses == "LOGINFAIL":
            #LOCAL username password failed
            pass;
        elif d1_ses == "TIMEOUT" or "UNKNOWN":
            #Not able to reach device1
            print "Unable to Reach :"+d1
        else:
            print "Unable to reach:"+d1

        
        if d1_stat != True:
            ibgp_ip = None
            print "Trying to login Secondary Device :"+d2+"\n"
            d2_ses = login(d2,[{"username":aaa_usr,"password":aaa_pass}])
            if type(d2_ses) != str or d2_ses != None:
                # Get IBGP IP and LOGIN TO SECONDARY DEVICE
                bgp = self.mod.check_bgp({"ssh_session":d2_ses[0],"vendor":"cisco","exp":d2_ses[1]})
                if type(bgp) != str and bgp != None:
                    for dat in bgp.get("data"):
                        if dat.get("bgptype") == "internal link" and dat.get("bgpstate") == "Established":
                            ibgp_ip = dat.get("neighbor")
                if ibgp_ip != None:
                    d1_ses = back_to_back({"device1":ibgp_ip,"ssh_session":d2_ses[0],"vendor":"cisco","exp":d2_ses[1],"auth":[{"username":aaa_usr,"password":aaa_pass}]})
                    if type(d1_ses) != str and d1_ses != None:
                        #Back to Back Login Success
                        self.check_device({"ssh_session":d1_ses[0],"vendor":"cisco","exp":d1_ses[1].strip()[:-1],"source_ip":d1})
                    else:
                        print "Back to Back Failed";
                else:
                    print "Not able to get IBGP ip"
            else:
                pass;
                #print "Unable to reach:"+d2+" Reasion:"+d2_ses
            d1_ses[0].close()







#ss = login('10.0.0.2','cisco','cisco')
d = {"username":"gowtham","password":"G0wtham@2","source_ip":"192.168.235.95","destination_ip":"192.168.233.3","proto":"ping","vendor":"cisco","ip":"192.168.233.3","device1":"192.168.235.95","device2":"192.168.204.68",}
#m = mod_check_end_to_end()
#print m.check_reach(d)
#print m.cis_check_arp(d)
#print m.cis_check_route(d)
#print m.check_bgp(d)
#print m.cisco_check_interface(d)
#ss[0].logout()
lm = lastMileCheck()
lm.try_ssh(d)


#########################################################################################################################################################
# TASK 1
def read_csv(usr_input="all"):
    try:
        csv_dic = {}
        csv_opn_hnd = open("/vagrant/mumbai_pr.csv","r")
        csv_read = csv.DictReader(csv_opn_hnd)
        if usr_input == "all":
            return csv_read
        for csv_data in csv_read:
            if str(csv_data).find(usr_input) != -1:
                print "Search Found:",csv_data.get("BANK-NAME")
                return csv_data
        return None
    except Excpetion as e:
        print "read_csv Error :"+str(e)
        return None

class main_t1():
    def __init__(self):
        self.session = None
        self.session_typ = None    
    # TASK 2
    def getinto(self,usr_input):
        try:
            
            # Login to target one router
            t1 = usr_input.get("target1")
            t2 = usr_input.get("target2")
            u1 = usr_input.get("username1")
            p1 = usr_input.get("password1")
            u2 = usr_input.get("username2")
            p2 = usr_input.get("password2")
            t1_status = None

            t1_chk = login(t1,u1,p1)
            if  t1_chk == "LOGINFAIL" :
                # Logged into system using crediantial 1 failed trying crediantial 2
                t1_chk = login(t1,u2,p2)
                if t1_chk == "LOGINFAIL":
                    return False
                elif type(t1_chk) == str:
                    # Not reachable
                    pass;
                else:
                   self.session = t1_chk
                   self.session_typ = "target1"
                   return True
            elif type(t1_chk) == str:
                # Not reachable
                pass;
            else:
                self.session = t1_chk
                self.session_typ = "target1"
                return True

            t2_chk = login(t2,u1,p1)
            if  t2_chk == "LOGINFAIL" :
                # Logged into system using crediantial 1 failed trying crediantial 2
                t2_chk = login(t2,u2,p2)
                if t2_chk == "LOGINFAIL":
                    return False
                elif type(t2_chk) == str:
                # Not reachable
                    pass;
                else:
                    self.session = t2_chk
                    self.session_typ = "target2"
                    return True
            elif type(t2_chk) == str:
                # Not reachable
                pass;
            else:
                self.session = t2_chk
                self.session_typ = "target2"
                return True
        except Exception as e:
            print e
        
    #TASK 3
    def trouble_shoot(self):
        # Cisco Search BGP PE IP
        pe_found = None
        pe_as = None
        self.session.sendline('show run | i neighbor')
        self.session.sendline('')
        self.session.expect(["#"],timeout=25)
        bgp_pe = self.session.before
        for pe in bgp_pe.split("\n"):
           if pe.find("4755") != -1 or  pe.find("18101") != -1 or  pe.find("9730") != -1:
               pe_found = pe.split("remote")[0].split("neighbor")[1].strip()
               pe_as = pe.split("remote-as")[1]
        print "\nPE INFO:"+str(pe_found)+" AS: "+pe_as
        if pe_found != None:
            self.session.before
            self.session.sendline('ping '+pe_found)
            self.session.expect([" ms"],timeout=10)
            #self.session.expect(["#"],timeout=5)
            print self.session.before
            self.session.sendline('show bgp neighbors '+pe_found+' | i BGP state')
            self.session.sendline("")
            self.session.expect(["#"],timeout=10)
            self.session.expect(["#"],timeout=10)
            print self.session.before
            self.session.logout()
    def start_run(self):
        bnk = raw_input("Enter Keyword to Search Bank :")
        usr = read_csv(bnk)
        if usr == None:
            print "Not able to find in database"
            return None
        else:
            getinto_status = self.getinto(usr)
            if getinto_status == True:
                self.trouble_shoot()


#m = main_t1()
#m.start_run()

