# Tasks
#TASK 0: -Get input from user: Bank name or ip address to troubleshoot.
#TASK 1: -Read CSV file and fetch bank info ( bank routers mgmt ip, backtoback ip, bank name,credentials).
#TASK 2: -Login using mgmt ip: if failed login to secondary router and login using back to back ip address ( first try aaa credentials if failed try local credentials )
#TASK 3: - (A)ping WAN ip address (B)check EBGP status & Return output.


from pexpect import pxssh
import pexpect
import getpass
import csv
import re

r_ip = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")


def login(hostname='',username='',password='',login_timeout=6,etimeout=5):
    # Login to NPCI device , "enable" password check disabled because of aaa conf in NPCI
    try:
        s = pxssh.pxssh(options={
                        "StrictHostKeyChecking": "no",
                        "UserKnownHostsFile": "/dev/null"},timeout=login_timeout)
        s.login(hostname, username, password,auto_prompt_reset=False,login_timeout=login_timeout)
        # Send enter to get router prompt to check login success
        s.sendline('')
        # expecting cisco , juniper , fortigate prompt 
        s.expect(["#",">","\$",pexpect.TIMEOUT],timeout=etimeout)
        login_chk = s.before
        if len(login_chk) > 0:
            host_name = str(login_chk.strip())
            aftr = s.after
            if type(aftr) == str:
                host_name = host_name+aftr.strip()
            print "Login Success :"+host_name
            return s,host_name
        else:
            print "Not able to predict login-success"
        return "UNKNOWN"
    except pxssh.ExceptionPxssh as e:
        err = str(e)
        if err.find("password refused") != -1:
            print "Login Failed"
            return "LOGINFAIL"
        else:
            print("Error>"+err)
        return "TIMEOUT"
    except Exception as e:
        #print("Unknown Error"+str(e))
        return "TIMEOUT"


class mod_check_end_to_end():
    def __init__(self):
        pass;
    def check_bgp_status(self):
        pass;
        
    def cis_check_route(self,d):
        try:
            if d.get("ip") != None:
                ven = d.get("vendor")
                ses = d.get("ssh_session")
                exp = d.get("exp")
                ses.sendline("show ip route "+d.get("ip"))
                ses.expect([exp,pxssh.TIMEOUT],timeout=20)
                v = ses.before
                r = re.findall(r"Known via \".*\"",v)[0]
                if len(r) > 0:
                    rr = r.replace("Known via \"","").replace("\"","")
                    if rr == 'connected':
                        rrr = re.findall(r"directly connected, via .*",v)[0].split("directly connected, via ")[1].strip()
                        return {"type":rr,"nxt_hop":rrr}
                    else:
                        rrr = re.findall(r"\* \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}",v)[0].replace("* ","").strip()
                        if rr.find("bgp") != -1:
                            tag = re.findall(r"Tag .*,",v)[0]
                            tag = tag[:-1].split("Tag ")[1]
                            return {"type":rr,"nxt_hop":rrr,"tag":tag}
                        else:
                            return {"type":rr,"nxt_hop":rrr}
        except Exception as e:
            print e

    def cis_check_arp(self,d):
        try:
            ven = d.get("vendor")
            ses = d.get("ssh_session")
            exp = d.get("exp")
            if d.get("ip") != None and exp != None:
                ses.sendline("show arp "+d.get("ip"))
                ses.expect([exp,pxssh.TIMEOUT],timeout=20)
                v = ses.before
                r = re.findall(r"[a-f|0-9]{4}\.[a-f|0-9]{4}\.[a-f|0-9]{4}",v)
                if len(r) > 0:
                    return {"type":"arp","arp":r[0]}
                else:
                    return {"type":"arp","arp":""}
        except Exception as e:
            print e
    
    def get_arp(self):
        pass;
    def check_reach(self,d):
        s = 0
        ses = d.get("ssh_session")
        src = d.get("source_ip")
        des = d.get("destination_ip")
        pro = d.get("proto")
        ven = d.get("vendor")
        exp = d.get("exp")
        if pro == "ping" and exp != None:
            if src == None:
                ses.sendline("ping "+des)
                ses.expect([exp,pxssh.TIMEOUT],timeout=20)
                s = 1
            else:
                ses.sendline("ping "+des+" source "+src)
                ses.expect([exp,pxssh.TIMEOUT],timeout=20)
                s = 1
            if s == 1:
                s = ses.before
                a = s.split("Success rate is ")
                if len(a) > 1:
                    return {"type":"ping","ping":a[1].split(" ")[0]}
            else:
                return None
    
    def check_bgp(self,d):
        try:
           ses = d.get("ssh_session")
           exp = d.get("exp")
           ven = d.get("vendor")
           data = []
           ses.sendline("show ip bgp neighbors | i BGP neighbor is ")
           ses.expect([exp,pxssh.TIMEOUT],timeout=20)
           s = ses.before
           ses.sendline("show ip bgp neighbors | i BGP state")
           ses.expect([exp,pxssh.TIMEOUT],timeout=20)
           ss = ses.before
           s1 = re.findall(r"BGP neighbor is .*link",s)
           s2 = re.findall(r"BGP state = .*",ss)
           if len(s1) == len(s2) and len(s1) > 0:
               for a in xrange(len(s1)):
                   c3 = ""
                   b1 = s1[a].split(",")
                   b2 = b1[0].replace("BGP neighbor is ","").strip()
                   b3 = b1[1].replace("  remote AS ","").strip()
                   b4 = b1[2].strip().strip()
                   c1 = s2[a].split(",")
                   c2 = c1[0].replace("BGP state = ","").strip()
                   if len(c1) > 1:
                       c3 = c1[1].replace(" up for ","").strip()
                   data.append({"neighbor":b2,"as":b3,"bgptype":b4,"bgpstate":c2,"bgpuptime":c3})
           data = {"type":"bgp","data":data}
           return data
        except Exception as e:
            print e

    def cisco_check_interface(self,d):
        try:
           ses = d.get("ssh_session")
           exp = d.get("exp")
           ven = d.get("vendor")
           data = []
           ses.sendline("show ip interface brief")
           ses.expect([exp,pxssh.TIMEOUT],timeout=20)
           ss = ses.before
           ss = ss.split("\n")[2:]  #Need to check this :-(
           for i in ss:
               mask = ""
               ii = ' '.join(i.split())
               ii = ii.split(" ")
               if len(ii) > 5:
                   interface = ii[0]
                   ip = ii[1]
                   admin = ii[4]
                   proto = ii[5]
                   if ip.find(".") != -1:
                       # Valid ip found check mask for the interface
                       ses.sendline("show interfaces "+interface+" | i "+ip)
                       ses.expect([exp,pxssh.TIMEOUT],timeout=20)
                       mask = ses.before
                       #print mask
                       mask = mask.split("/")[1]
                       print mask
                   data.append({"interface":interface,"ip_addr":ip,"admin_stat":admin,"protocol_stat":proto,"mask":mask})
           data = {"type":"interface","data":data}
           #return data
        except Exception as e:
            print e
    def check_source_route(self):
        pass;
    def check_dest_route(self):
        pass;
    
        
        
        









ss = login('192.168.204.238','gowtham','G0wtham@2')
d = {"source_ip":"192.168.204.238","ssh_session":ss[0],"exp":ss[1],"destination_ip":"192.168.180.120","proto":"ping","vendor":"cisco","ip":"192.168.159.29"}
m = mod_check_end_to_end()
#print m.check_reach(d)
#print m.cis_check_arp(d)
#print m.cis_check_route(d)
#print m.check_bgp(d)
print m.cisco_check_interface(d)
ss[0].logout()



#########################################################################################################################################################
# TASK 1
def read_csv(usr_input="all"):
    try:
        csv_dic = {}
        csv_opn_hnd = open("/vagrant/mumbai_pr.csv","r")
        csv_read = csv.DictReader(csv_opn_hnd)
        if usr_input == "all":
            return csv_read
        for csv_data in csv_read:
            if str(csv_data).find(usr_input) != -1:
                print "Search Found:",csv_data.get("BANK-NAME")
                return csv_data
        return None
    except Excpetion as e:
        print "read_csv Error :"+str(e)
        return None

class main_t1():
    def __init__(self):
        self.session = None
        self.session_typ = None    
    # TASK 2
    def getinto(self,usr_input):
        try:
            
            # Login to target one router
            t1 = usr_input.get("target1")
            t2 = usr_input.get("target2")
            u1 = usr_input.get("username1")
            p1 = usr_input.get("password1")
            u2 = usr_input.get("username2")
            p2 = usr_input.get("password2")
            t1_status = None

            t1_chk = login(t1,u1,p1)
            if  t1_chk == "LOGINFAIL" :
                # Logged into system using crediantial 1 failed trying crediantial 2
                t1_chk = login(t1,u2,p2)
                if t1_chk == "LOGINFAIL":
                    return False
                elif type(t1_chk) == str:
                    # Not reachable
                    pass;
                else:
                   self.session = t1_chk
                   self.session_typ = "target1"
                   return True
            elif type(t1_chk) == str:
                # Not reachable
                pass;
            else:
                self.session = t1_chk
                self.session_typ = "target1"
                return True

            t2_chk = login(t2,u1,p1)
            if  t2_chk == "LOGINFAIL" :
                # Logged into system using crediantial 1 failed trying crediantial 2
                t2_chk = login(t2,u2,p2)
                if t2_chk == "LOGINFAIL":
                    return False
                elif type(t2_chk) == str:
                # Not reachable
                    pass;
                else:
                    self.session = t2_chk
                    self.session_typ = "target2"
                    return True
            elif type(t2_chk) == str:
                # Not reachable
                pass;
            else:
                self.session = t2_chk
                self.session_typ = "target2"
                return True
        except Exception as e:
            print e
        
    #TASK 3
    def trouble_shoot(self):
        # Cisco Search BGP PE IP
        pe_found = None
        pe_as = None
        self.session.sendline('show run | i neighbor')
        self.session.sendline('')
        self.session.expect(["#"],timeout=25)
        bgp_pe = self.session.before
        for pe in bgp_pe.split("\n"):
           if pe.find("4755") != -1 or  pe.find("18101") != -1 or  pe.find("9730") != -1:
               pe_found = pe.split("remote")[0].split("neighbor")[1].strip()
               pe_as = pe.split("remote-as")[1]
        print "\nPE INFO:"+str(pe_found)+" AS: "+pe_as
        if pe_found != None:
            self.session.before
            self.session.sendline('ping '+pe_found)
            self.session.expect([" ms"],timeout=10)
            #self.session.expect(["#"],timeout=5)
            print self.session.before
            self.session.sendline('show bgp neighbors '+pe_found+' | i BGP state')
            self.session.sendline("")
            self.session.expect(["#"],timeout=10)
            self.session.expect(["#"],timeout=10)
            print self.session.before
            self.session.logout()
    def start_run(self):
        bnk = raw_input("Enter Keyword to Search Bank :")
        usr = read_csv(bnk)
        if usr == None:
            print "Not able to find in database"
            return None
        else:
            getinto_status = self.getinto(usr)
            if getinto_status == True:
                self.trouble_shoot()


#m = main_t1()
#m.start_run()

